const tableColumns = [
  {
    title: '微信昵称',
    dataIndex: 'nickname',
    align: 'center'
  },
  {
    title: '学习课程/练习',
    dataIndex: 'course_name',
    align: 'center'
  },
  {
    title: '我是小老师',
    dataIndex: 'teacher_review',
    scopedSlots: { customRender: 'teacher_review' },
    align: 'center'
  },
  {
    title: '是否点评',
    dataIndex: 'teacher_voice',
    width: 100,
    scopedSlots: { customRender: 'teacher_voice' },
    align: 'center'
  },
  {
    title: '操作',
    dataIndex: 'action',
    scopedSlots: { customRender: 'action' },
    align: 'center'
  }
]

const fieldOptions = [
  {
    type: 'input',
    label: '用户ID',
    decorator: ['user_id']
  },
  {
    type: 'input',
    label: '第几课',
    decorator: ['num']
  }
]

export { tableColumns, fieldOptions }
