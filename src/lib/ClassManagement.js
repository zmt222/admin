const tableColumns = [
  {
    title: '开班渠道',
    dataIndex: 'market_name'
  },
  {
    title: '课程名称',
    dataIndex: 'course_name'
  },
  {
    title: '课程期数/班级',
    dataIndex: 'course_class'
  },
  {
    title: '学员人数',
    dataIndex: 'learn_people'
  },
  {
    title: '跟上进度数',
    dataIndex: 'meet_people'
  },
  {
    title: '当前学习概况',
    dataIndex: 'learn_situation'
  },
  {
    title: '当前课程/练习',
    dataIndex: 'learn_info',
    width: 130,
    scopedSlots: { customRender: 'learn_info' }
  },
  {
    title: '添加老师',
    dataIndex: 'add_teacher'
  },
  {
    title: '转化人数',
    dataIndex: 'reverse_people'
  },
  {
    title: '开课状态',
    dataIndex: 'classes_type',
    width: 130,
    scopedSlots: { customRender: 'classes_type' }
  },
  {
    title: '操作',
    dataIndex: 'action',
    width: 130,
    scopedSlots: { customRender: 'action' }
  }
]

const fieldOptions = [
  {
    type: 'input',
    label: '班级期数',
    decorator: ['course_class']
  },
  {
    type: 'select',
    label: '开课状态',
    decorator: ['classes_type'],
    selectOptions: [
      {
        value: 0,
        text: '未开课'
      },
      {
        value: 1,
        text: '开课中'
      },
      {
        value: 2,
        text: '已结课'
      }
    ]
  },
  {
    type: 'select',
    label: '课程渠道',
    decorator: ['market_name'],
    selectOptions: []
  },
  {
    type: 'select',
    label: '课程名称',
    decorator: ['course_name'],
    selectOptions: []
  }
]

export { tableColumns, fieldOptions }
