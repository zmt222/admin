const tableColumns = [
  {
    title: '渠道来源',
    dataIndex: 'market_name'
  },
  {
    title: '微信昵称',
    dataIndex: 'nickname'
  },
  {
    title: '用户昵称',
    dataIndex: 'baby_name'
  },
  {
    title: '手机号',
    dataIndex: 'phone'
  },
  {
    title: '当课是否学课/练习',
    dataIndex: 'learn_practise',
    scopedSlots: { customRender: 'learn_practise' }
  },
  {
    title: '累计学课/练习',
    dataIndex: 'count_learn_practise',
    width: 80,
    scopedSlots: { customRender: 'count_learn_practise' }
  },
  {
    title: '当前点评状态',
    dataIndex: 'review_type',
    scopedSlots: { customRender: 'review_type' }
  },
  {
    title: '是否添加老师',
    dataIndex: 'wechat_switch',
    scopedSlots: { customRender: 'wechat_switch' }
  },
  {
    title: '转化状态',
    dataIndex: 'transform_type',
    width: 120,
    scopedSlots: { customRender: 'transform_type' }
  },
  {
    title: '首次转化课程',
    dataIndex: 'transform_course'
  },
  {
    title: '操作',
    dataIndex: 'action',
    width: 180,
    scopedSlots: { customRender: 'action' }
  }
]

const fieldOptions = [
  {
    type: 'input',
    label: '学员手机号',
    decorator: ['phone']
  },
  {
    type: 'select',
    label: '期数',
    decorator: ['classStatus'],
    selectOptions: []
  },
  {
    type: 'select',
    label: '添加老师',
    decorator: ['classStatus1'],
    selectOptions: [
      {
        value: 0,
        text: '否'
      },
      {
        value: 1,
        text: '是'
      }
    ]
  },
  {
    type: 'input',
    label: '学员ID',
    decorator: ['id']
  },
  {
    type: 'input',
    label: '微信昵称',
    decorator: ['nickname__contains']
  },
  {
    type: 'select',
    label: '转化状态',
    decorator: ['transform_type'],
    selectOptions: [
      {
        value: 0,
        text: '未转化'
      },
      {
        value: 1,
        text: '已转化'
      },
      {
        value: 2,
        text: '2次转化'
      }
    ]
  }
]

export {
  tableColumns,
  fieldOptions
}
