const tableColumns = [
  {
    title: '微信昵称',
    dataIndex: 'nickname'
  },
  {
    title: '手机号',
    dataIndex: 'phone'
  },
  {
    title: '注册时间',
    dataIndex: 'create_time',
    width: 170,
    scopedSlots: { customRender: 'create_time' }
  },
  {
    title: '真实学习阶段',
    dataIndex: 'true_stage'
  },
  {
    title: '当课是否学课/练习',
    dataIndex: 'learn_practise',
    scopedSlots: { customRender: 'learn_practise' }
  },
  {
    title: '累计学课/练习',
    dataIndex: 'count_learn_practise'
  },
  {
    title: '是否赠课',
    dataIndex: 'is_free_course',
    scopedSlots: { customRender: 'is_free_course' }
  },
  {
    title: '转化状态',
    dataIndex: 'transform_type',
    width: 120,
    scopedSlots: { customRender: 'transform_type' }
  },
  {
    title: '首次转化课程',
    dataIndex: 'transform_course'
  },
  {
    title: '操作',
    dataIndex: 'action',
    width: 130,
    scopedSlots: { customRender: 'action' }
  }
]

const fieldOptions = [
  {
    type: 'input',
    label: '学员手机号',
    decorator: ['phone']
  },
  {
    type: 'select',
    label: '转化状态',
    decorator: ['transform_type'],
    selectOptions: [
      {
        value: 0,
        text: '未转化'
      },
      {
        value: 1,
        text: '已转化'
      },
      {
        value: 2,
        text: '2次转化'
      }
    ]
  },
  {
    type: 'input',
    label: '学员ID',
    decorator: ['id']
  },
  {
    type: 'date-picker',
    label: '注册时间',
    decorator: ['create_time']
  },
  {
    type: 'input',
    label: '微信昵称',
    decorator: ['nickname__contains']
  },
  {
    type: 'select',
    label: '是否赠课',
    decorator: ['is_free_course'],
    selectOptions: [
      {
        value: 0,
        text: '否'
      },
      {
        value: 1,
        text: '是'
      }
    ]
  }
]

export { tableColumns, fieldOptions }
