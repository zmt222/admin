const tableColumns = [
  {
    title: '购买课程',
    dataIndex: 'contractNo'
  },
  {
    title: '原价',
    dataIndex: 'customerName'
  },
  {
    title: '优惠券',
    dataIndex: 'serviceCompanyName'
  },
  {
    title: '魔术币',
    dataIndex: 'serviceFeeName'
  },
  {
    title: '实付金额',
    dataIndex: 'prePayFeeName'
  },
  {
    title: '支付时间',
    dataIndex: 'contractStartDate'
  },
  {
    title: '是否有礼',
    dataIndex: 'contractEndDate'
  },
  {
    title: '转化次数',
    dataIndex: 'archiveStatusName'
  },
  {
    title: '转化方式',
    dataIndex: 'lastUpdateAt'
  }
]

export default tableColumns
