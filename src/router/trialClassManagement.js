const RouterView = () => import('./router_view.vue')

const trialClassManagement = {
  path: '/trialClassManagement',
  component: RouterView,
  children: [
    {
      path: 'classList',
      component: () => import('@/views/TrialClassManagement/ClassManagement/index.vue')
    },
    {
      path: 'classDetail',
      component: () => import('@/views/TrialClassManagement/ClassManagement/classDetail.vue')
    },
    {
      path: 'studentList',
      component: () => import('@/views/TrialClassManagement/TrialStudentManagement/index.vue')
    },
    {
      path: 'studentDetail',
      component: () => import('@/views/TrialClassManagement/TrialStudentManagement/studentDetail.vue')
    },
    {
      path: 'openClassManagement',
      component: () => import('@/views/TrialClassManagement/OpenClassManagement/index.vue')
    },
    {
      path: 'addClass',
      component: () => import('@/views/TrialClassManagement/OpenClassManagement/addClass.vue')
    },
    {
      path: 'exerciseList',
      component: () => import('@/views/TrialClassManagement/ExerciseList/index.vue')
    }
  ]
}

export default trialClassManagement
