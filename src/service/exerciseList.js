/**
 * 体验课练习列表
 */

import { post } from './index'

function getExerciseList(params = {}) {
  const url = '/backstage/admin/text_trial_exercise_list'
  return post(url, params)
}

function updateVoice(params) {
  const url = '/backstage/admin/update_voice'
  return post(url, params)
}

export { getExerciseList, updateVoice }
