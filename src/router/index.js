import Vue from 'vue'
import VueRouter from 'vue-router'
import trialClassManagement from './trialClassManagement'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/index'
  },
  {
    path: '/index',
    component: () => import('@/views/AdvisorStuManagement/index.vue')
  },
  trialClassManagement
]

const router = new VueRouter({
  routes
})

export default router
