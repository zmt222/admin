/**
 * 体验班学习管理-班级列表
 */

import { post } from './index'

// 查询班级列表
function getClassList(params = {}) {
  const url = '/backstage/admin/trial_class_learn'
  return post(url, params)
}

// 查询班级详情
function getClassDetail(params = {}) {
  const url = '/backstage/admin/learn_overview'
  return post(url, params)
}

export { getClassList, getClassDetail }
