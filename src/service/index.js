import axios from 'axios'
import { notification } from 'ant-design-vue'

const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL,
  timeout: 20000
})

// 添加请求拦截器
service.interceptors.request.use(
  config => {
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

// 添加响应拦截器
service.interceptors.response.use(
  response => {
    const { code } = response.data
    if (code !== 200) {
      if (code === 401) {
        notification({ message: '登录超时，请重新登录!' })
        // todo: 登出
      }
      return Promise.reject(response.data)
    }
    return response.data
  },
  error => {
    if (error.response) {
      const { status } = error.response
      switch (status) {
        case 401:
          notification.error({ message: '登录超时，请重新登录！' })
          // todo: 登出
          break
        case 500:
          notification.error({ message: '服务器异常！' })
          break
        default:
          notification({ message: error.message })
          break
      }
    } else {
      notification({ message: '网络异常' })
    }
    return Promise.reject(error)
  }
)

function get(url, params = {}, config = {}) {
  return service.get(url, { params, ...config })
}

function post(url, params = {}, config = {}) {
  return service.post(url, params, config)
}
function upload(url, params = {}, config = {}) {
  return service.post(url, params, {
    ...config,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

export {
  get,
  post,
  upload
}
