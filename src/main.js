import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import antd from './utils/ant-design'
// import 'ant-design-vue/dist/antd.css'
import { get, post, upload } from './service/index'
import { formatList, jsonTransform } from '@/utils/tools'
import formSearch from '@/components/formSearch'
import moment from 'moment'

Vue.use(antd)

Vue.config.productionTip = false
Vue.prototype.$get = get
Vue.prototype.$post = post
Vue.prototype.$upload = upload
Vue.prototype.$formatList = formatList
Vue.prototype.$jsonTransform = jsonTransform
Vue.prototype.$moment = moment
Vue.component('form-search', formSearch)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
