/**
 * 格式化列表，将空字段内容使用'-'代替
 * @param {Array} list 被格式化的列表数据
 * @param {Array} whileList key值白名单，whileList中的key将跳过校验不做处理
 */
function formatList(list, whileList = []) {
  list.map(item => {
    Object.keys(item).forEach(key => {
      if (whileList.includes(key)) return
      if (item[key] === '' || item[key] === null) {
        item[key] = '-'
      }
    })
    return item
  })
}

/**
 * 简单深拷贝，去掉值为undefined的键值对
 * @param {Array} params 被拷贝的对象
 */
function jsonTransform(params = {}) {
  return JSON.parse(JSON.stringify(params))
}

export { formatList, jsonTransform }
