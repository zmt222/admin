const path = require('path')
function resolve(dir) {
  return path.join(__dirname, dir)
}

module.exports = {
  publicPath: 'http://backend.moshujiamm.com/',
  lintOnSave: true,
  devServer: {
    proxy: {
      '/backstage': {
        target: 'http://10.0.0.200:8080',
        pathRewrite: {
          '^/backstage': '/backstage'
        },
        changeOrigin: true
      },
      '/v2_revision': {
        target: 'http://10.0.0.200:8080',
        pathRewrite: {
          '^/v2_revision': '/v2_revision'
        },
        changeOrigin: true
      },
      '/user': {
        target: 'http://api.moshujiamm.com',
        changeOrigin: true
      }
    }
  },
  css: {
    loaderOptions: {
      scss: {
        prependData: '@import "./src/assets/style/index.scss";'
      },
      less: {
        lessOptions: {
          javascriptEnabled: true
        }
      }
    }
  },
  configureWebpack: {
    resolve: {
      alias: {
        '@': resolve('src')
      }
    }
  }
}
