/**
 * 课程顾问学员管理
 */

import { post, get } from './index'

// 查询学员列表
function getStudentList(params = {}) {
  const url = '/backstage/admin/consultant_user'
  return post(url, params)
}

// 注销账户
function deleteUser(params = {}) {
  const url = '/user/index/deleteuser'
  return post(url, params)
}

// 查询课程
function getCourse(params = {}) {
  const url = '/backstage/adminfree_course_show'
  return post(url, params)
}

// 赠送免费课程
function getFreeCourse(params = {}) {
  const url = '/v2_revision/update_free_course'
  return post(url, params)
}

// 查询账户
function getUserInfo(params = {}) {
  const url = '/backstage/admin/get_create_user'
  return get(url, params)
}

// 创建账户
function createUser(params = {}) {
  const url = '/backstage/admin/get_create_user'
  return post(url, params)
}

export {
  getStudentList,
  deleteUser,
  getCourse,
  getFreeCourse,
  getUserInfo,
  createUser
}
