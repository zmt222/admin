const tableColumns = [
  {
    title: '官方时间',
    dataIndex: 'contractNo'
  },
  {
    title: '学习课程/练习',
    dataIndex: 'customerName'
  },
  {
    title: '是否学习/练习',
    dataIndex: 'serviceCompanyName'
  },
  {
    title: '学习时间',
    dataIndex: 'serviceFeeName'
  },
  {
    title: '练习时间',
    dataIndex: 'prePayFeeName'
  },
  {
    title: '获得等级',
    dataIndex: 'contractStartDate'
  },
  {
    title: '获得能量',
    dataIndex: 'currentClass',
    width: 130,
    scopedSlots: { customRender: 'currentClass' }
  },
  {
    title: '我是小老师',
    dataIndex: 'littleTeacher',
    scopedSlots: { customRender: 'littleTeacher' }
  },
  {
    title: '是否点评',
    dataIndex: 'classStatus',
    width: 130,
    scopedSlots: { customRender: 'classStatus' }
  },
  {
    title: '操作',
    dataIndex: 'action',
    width: 130,
    scopedSlots: { customRender: 'action' }
  }
]

export default tableColumns
