const tableColumns = [
  {
    title: '渠道名称',
    dataIndex: 'contractNo'
  },
  {
    title: '课程名称',
    dataIndex: 'customerName'
  },
  {
    title: '期数',
    dataIndex: 'serviceCompanyName'
  },
  {
    title: '班数',
    dataIndex: 'serviceFeeName'
  },
  {
    title: '开班时间',
    dataIndex: 'prePayFeeName'
  },
  {
    title: '结课时间',
    dataIndex: 'contractStartDate'
  },
  {
    title: '开售时间',
    dataIndex: 'currentClass'
  },
  {
    title: '截止售卖时间',
    dataIndex: 'archiveStatusName'
  },
  {
    title: '加入人数',
    dataIndex: 'lastUpdateAt'
  },
  {
    title: '目前状态',
    dataIndex: 'classStatus',
    width: 130,
    scopedSlots: { customRender: 'classStatus' }
  },
  {
    title: '操作',
    dataIndex: 'action',
    width: 130,
    scopedSlots: { customRender: 'action' }
  }
]

export default tableColumns
