const menuList = [
  {
    id: 1,
    name: '课程顾问学员管理',
    router: '/index',
    icon: 'menu01',
    children: []
  },
  {
    id: 2,
    name: '体验班学习管理',
    router: '/trialClassManagement',
    icon: 'menu02',
    children: [
      {
        id: 3,
        name: '班级列表',
        router: '/trialClassManagement/classList',
        icon: 'menu03',
        children: []
      },
      {
        id: 4,
        name: '体验班学员列表',
        router: '/trialClassManagement/studentList',
        icon: 'menu03',
        children: []
      },
      // {
      //   id: 5,
      //   name: '开课课程管理',
      //   router: '/trialClassManagement/openClassManagement',
      //   icon: 'menu03',
      //   children: []
      // },
      {
        id: 6,
        name: '体验课练习管理',
        router: '/trialClassManagement/exerciseList',
        icon: 'menu03',
        children: []
      }
    ]
  }
]
export default menuList
